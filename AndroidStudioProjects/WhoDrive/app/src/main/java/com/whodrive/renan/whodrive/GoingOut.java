package com.whodrive.renan.whodrive;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class GoingOut implements Serializable {
    private String mDriversName;
    private String mMembersMeeting;

    private String  mNumberOfCars;

    private String mDate;
    transient private Bitmap mPicture;

    public GoingOut(String mDriversName, String mMembersMeeting, String mNumberOfCars, String mDate, Bitmap mPicture) {
        this.mDriversName = mDriversName;
        this.mMembersMeeting = mMembersMeeting;
        this.mNumberOfCars = mNumberOfCars;
        this.mDate = mDate;
        this.mPicture = mPicture;
    }

    public String getmDriversName() {
        return mDriversName;
    }

    public String getmMembersMeeting() {
        return mMembersMeeting;
    }

    public String getmNumberOfCars() {
        return mNumberOfCars;
    }

    public String getmDate() {
        return mDate;
    }

    public Bitmap getmPicture() {
        return mPicture;
    }

    public void setmPicture(Bitmap mPicture) {
        this.mPicture = mPicture;
    }


/*
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        mPicture.compress(Bitmap.CompressFormat.JPEG,50,out);
            out.defaultWriteObject();
    }
*/
  private void writeObject(ObjectOutputStream aOutputStream) throws IOException {
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      mPicture.compress(Bitmap.CompressFormat.PNG, 50, stream);
      aOutputStream.writeObject(stream.toByteArray());
      aOutputStream.defaultWriteObject();
  }

    /*private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        mPicture = BitmapFactory.decodeStream(in);
        in.defaultReadObject();
    }*/
 private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
     byte[] stream = (byte[]) aInputStream.readObject();
     mPicture = BitmapFactory.decodeByteArray(stream, 0, stream.length);
     aInputStream.defaultReadObject();
 }
}
